angular.module('dZoneStatistics.stats')
    .service("statsService", ['GLOBAL', '$http', '$q',
        function (GLOBAL, $http, $q) {
            'use strict';

            var service = {};

            service.getAllDzonePages = function (id, pages) {
                var deferred = $q.defer();
                var returnArray = [];
                var pagesRetrieved = 0;

                $q.all(pages.map(function (page) {
                    return $http.get(GLOBAL.URL_VIEW_BASE + GLOBAL.URL_BASE + id + GLOBAL.URL_PAGE + page + GLOBAL.URL_PORTAL_SORT)
                        .then(function (response) {
                            if (response.data.result.data.nodes.length > 0) {
                                for (var i = 0; i < response.data.result.data.nodes.length; i++) {
                                    returnArray.push(response.data.result.data.nodes[i]);
                                }

                                pagesRetrieved++;
                            }
                        });
                })).then(function () {
                    console.log("returnArray", returnArray);
                    console.log("pagesRetrieved", pagesRetrieved);
                    deferred.resolve(returnArray);
                });

                return deferred.promise;
            };

            return service;
        }]);