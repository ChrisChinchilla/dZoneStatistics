angular.module('dZoneStatistics.stats')
    .controller("statsCtrl", ['statsService', '$location', 'GLOBAL',
        function (statsService, $location, GLOBAL) {
            'use strict';

            var vm = this;
            vm.data = [];
            vm.articleCount = 0;
            vm.totalPageViews = 0;
            vm.averagePageViewsPerArticle = 0;
            vm.portalList = [];
            vm.views = [];
            vm.names = [];
            vm.realName = "";
            vm.avatarURL = "";
            vm.averageViewsPerDay = 0;
            vm.firstArticlePubDate = moment();
            vm.baseURL = GLOBAL.URL_VIEW_BASE;

            // TODO: move code to service
            // TODO: check for max records and throw warning
            // TODO: hop out when zero records returned
            // TODO: look into pushing to AWS free
            // TODO: better way to get user info (instead of first article, first author)

            var today = moment();

            function getDzoneAllPageInfo(id, maxPageNum) {
                vm.data = [];
                var pages = [];

                for (var i = 1; i <= maxPageNum; i++) {
                    pages[i-1] = i;
                }

                statsService.getAllDzonePages(id, pages).then(function (response) {
                    if (response) {
                        vm.data = response;
                        vm.articleCount = vm.data.length;
                        getPageViewStats(vm.data);
                        computeAverages();
                        computeFinalStats();
                        getNameAndAvatar(vm.data[0]);
                    }
                });
            }

            function getNameAndAvatar(data) {
                vm.realName = data.authors[0].realName;
                vm.avatarURL = "https://" + data.authors[0].avatar;
            }

            function getPageViewStats(data) {
                var viewTotal = 0;

                for (var i = 0; i < data.length; i++) {
                    var thisData = data[i];
                    determineOldestArticle(thisData);
                    buildPortalList(thisData);
                    viewTotal = viewTotal + thisData.views;
                }

                vm.totalPageViews = viewTotal;

                if (viewTotal > 0) {
                    vm.averagePageViewsPerArticle = viewTotal / data.length;
                }
            }

            function determineOldestArticle(thisData) {
                var thisArticleDate = moment(thisData.articleDate);

                if (moment(thisArticleDate).isBefore(vm.firstArticlePubDate)) {
                    vm.firstArticlePubDate = thisArticleDate;
                }
            }

            function buildPortalList(thisData) {
                var portal = [];
                if (vm.portalList && vm.portalList.length > 0) {
                    var found = false;
                    for (var i = 0; i < vm.portalList.length; i++) {
                        var thisPortal = vm.portalList[i];

                        if (thisPortal.name.toUpperCase().trim() === thisData.portal.toUpperCase().trim()) {
                            found = true;
                            thisPortal.views = thisPortal.views + thisData.views;
                            thisPortal.count = thisPortal.count + 1;
                            break;
                        }
                    }

                    if (!found) {
                        portal.name = thisData.portal;
                        portal.count = 1;
                        portal.views = thisData.views;
                        portal.avgViews = 0;
                        vm.portalList.push(portal);
                    }
                } else {
                    portal.name = thisData.portal;
                    portal.count = 1;
                    portal.views = thisData.views;
                    portal.avgViews = 0;
                    vm.portalList.push(portal);
                }
            }

            function computeAverages() {
                for (var i = 0; i < vm.portalList.length; i++) {
                    var thisPortal = vm.portalList[i];
                    vm.views.push(thisPortal.views);
                    vm.names.push(thisPortal.name);
                    thisPortal.avgViews = thisPortal.views / thisPortal.count;
                }
            }

            function computeFinalStats() {
                vm.daysSinceFirstPost = today.diff(vm.firstArticlePubDate, "days");
                vm.averageViewsPerDay = vm.totalPageViews / vm.daysSinceFirstPost;
            }

            function init() {
                var id = $location.search().id;

                if (!id) {
                    // if no id,
                    id = 770327;
                }

                getDzoneAllPageInfo(id, 25);
            }

            init();

    }]);