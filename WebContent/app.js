'use strict';

angular.module('dZoneStatistics', ['smart-table', 'ngRoute', 'dZoneStatistics.stats'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider.otherwise({redirectTo: '/stats'});
    }]);