## dZoneStatistics


[![build status](https://gitlab.com/johnjvester/dZoneStatistics/badges/master/build.svg)](https://gitlab.com/johnjvester/dZoneStatistics/commits/master)

> The dZoneStatistics project was created so that I could view additional analytics regarding my monthly 
> articles as a Zone Leader for DZone.com. 
>
> The project is an AngularJS static web project, relying on a public RESTful API hosted by DZone.com.  The 
> single page application retrieves the articles (and metadata) from the user's DZone ID (which can be found on the user's DZone profile page) and then performs some basic analysis of the data.

### About the Code

A few weeks ago on our Slack channel, one of the other Zone Leaders mentioned finding a RESTful end-point 
that provides details behind an author's posts on DZone.  This piqued my interest, as I have always wondered 
what additional statistics I could locate for my articles.

Since I have been working on an AngularJS project, I thought I would use AngularJS to create a simple application 
to present my DZone post analytics.  

The very simple project uses an AngularJS module called `stats`, which is located in the `WebContent/stats` folder. That 
module leverages a `statsCtrl.js` (controller) to communicate with the view - which is the `stats.html` file used 
to present data to the consumer.  The controller relies on a `statsService.js` (service) class to make the necessary API 
calls to retrieve the data from DZone.com.

### Live Preview

If you want to see the project in action, simply use the following URL (hosted by GitLab Pages):

[https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=770327](https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=770327)

The link above will present DZone Statistics for API Evangelist, Kin Lane.

### More Information

This project inspired a how-to article at DZone.com, which can be viewed at the following URL:

[Tutorial: How a Web Application Can Consume Data From a Web API](https://dzone.com/articles/tutorial-how-a-web-application-can-consume-data-fr)

Created by [John Vester](https://www.linkedin.com/in/johnjvester), because I truly enjoy writing code. 